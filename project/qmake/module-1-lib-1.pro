TEMPLATE = lib
CONFIG *= static

HEADERS *= $${PWD}/../../src/lib-1/first_lib.h

SOURCES *= $${PWD}/../../src/lib-1/first_lib.cpp

OTHER_FILES *= $${PWD}/module-1-lib-1.prf
